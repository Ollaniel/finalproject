package by.epam.javatr.rdbms.controller.command.implementation;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.javatr.rdbms.bean.Request;
import by.epam.javatr.rdbms.controller.Validator;
import by.epam.javatr.rdbms.controller.command.Command;
import by.epam.javatr.rdbms.dao.factory.ConnectionPool;
import by.epam.javatr.rdbms.service.RequestService;
import by.epam.javatr.rdbms.service.exception.ServiceException;
import by.epam.javatr.rdbms.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

/***
 * Command for request creation.
 * There could be two basic options - success creation or conflicts with free beds. 
 * If case of conflict exception is rising and if it specific exception 
 * with conflict info been filled then special page shows to display conflict details.
 * That page should use custom tags.  
 * For input parameter validation we use Validator class
 * @see /finaltask/src/by/epam/javatr/rdbms/controller/Validator.java 
 * @author Sergii_Kotov
 *
 */
public class RequestCreate implements Command{
	private static final Logger logger = Logger.getLogger(RequestCreate.class.getName());

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) {
	try {
		ServiceFactory sf = ServiceFactory.getInstance();
		RequestService mService = sf.getRequestService();
		try {
			logger.debug("go deeper to service - req create");

			// создаем запрос. его нужо будет брать из req
			List<Integer> beds = new ArrayList <Integer>();
			// проверка cтроки со списком мест. Если не то - исключение IllegalArgumentException
			Validator v=new Validator();
			beds=v.validateBeds(req.getParameter("nbeds"));
			
			Date dtS=null;
			Date dtE=null;
			dtS = v.validateDate(req.getParameter("dtstart"));
			dtE = v.validateDate(req.getParameter("dtend"));
			
			int typeB=v.validateType(req.getParameter("typeBook"));
			
			// юзер - пока заглушка. 
			Request r = new Request(1,typeB,dtS,dtE,beds);
			
			req.setAttribute("hotelrequest", mService.createRequest(r));
			logger.debug("return from service");
			req.getRequestDispatcher("/jsp/requestDone.jsp").forward(req, resp);
			
		} catch (ServiceException e) {
			// штатное исключение - заказ пересекается с существующими
			if (e.getConflicts()!=null) {
				logger.debug("conflicts are on the service level");
				// output with custom tag
				req.setAttribute("requestconfl", e.getConflicts());
				
				//call for custom tag there
				req.getRequestDispatcher("/jsp/requestSpoiled.jsp").forward(req, resp);
			} else {
				req.getRequestDispatcher("/jsp/error.jsp").forward(req, resp);
			}
		} catch (IllegalArgumentException e) { 
			// ошибки валидации
			logger.error("validation error ");
			// они выводятся внизу формы ввода
			req.setAttribute("errorText", e.getMessage());
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
		}
	} catch (ServletException | IOException e) {
		logger.error(e);
	}
	}

	
}
