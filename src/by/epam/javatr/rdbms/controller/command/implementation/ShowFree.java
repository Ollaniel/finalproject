package by.epam.javatr.rdbms.controller.command.implementation;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.javatr.rdbms.controller.command.Command;
import by.epam.javatr.rdbms.dao.factory.DaoFactory;
import by.epam.javatr.rdbms.service.MutualInfoService;
import by.epam.javatr.rdbms.service.exception.ServiceException;
import by.epam.javatr.rdbms.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

/***
 * Implementation of Command 
 * for requesting state of each bed. 
 * That to be displayed to users and to let them pick up beds.
 * @author Sergii_Kotov
 *
 */
public class ShowFree implements Command{
	private static final Logger logger = Logger.getLogger(ShowFree.class.getName());

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) {
		try {
			ServiceFactory sf = ServiceFactory.getInstance();
			MutualInfoService mService = sf.getMutInfService();
			try {
				logger.debug("go deeper to a service");
				req.setAttribute("bedsList", mService.showFreeBeds());
				logger.debug("return from service");
			} catch (ServiceException e) {
				logger.error(e);
			}
			req.getRequestDispatcher("/jsp/freebed.jsp").forward(req, resp);
		} catch (ServletException | IOException e) {
			logger.error(e);
			try {
				req.getRequestDispatcher("/jsp/error.jsp").forward(req, resp);
			} catch(Exception ee) {
				logger.error(e);
			}
		}
	}

}
