package by.epam.javatr.rdbms.service;


import java.util.List;

import by.epam.javatr.rdbms.bean.BedPriceParametrs;
import by.epam.javatr.rdbms.bean.FreeBed;
import by.epam.javatr.rdbms.service.exception.ServiceException;

public interface MutualInfoService {
	public List<FreeBed> showFreeBeds() throws ServiceException ;
	public double get_request_sum(int id) throws ServiceException;
	public BedPriceParametrs getTodayPrice() throws ServiceException;
}
