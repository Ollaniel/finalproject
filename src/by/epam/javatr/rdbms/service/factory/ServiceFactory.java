package by.epam.javatr.rdbms.service.factory;

import by.epam.javatr.rdbms.service.*;
import by.epam.javatr.rdbms.service.implementation.MutualInfoServiceImpl;
import by.epam.javatr.rdbms.service.implementation.RequestServiceImpl;

public class ServiceFactory {
	private static final ServiceFactory instance = new ServiceFactory();
	private final MutualInfoService mInfService = new MutualInfoServiceImpl();
	private final RequestService reqService = new RequestServiceImpl();
	private ServiceFactory(){}
	public static ServiceFactory getInstance(){
	return instance;
	}
	public MutualInfoService getMutInfService(){
		return mInfService;
	}
	public RequestService getRequestService(){
		return reqService;
	}
}
