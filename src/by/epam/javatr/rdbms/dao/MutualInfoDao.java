package by.epam.javatr.rdbms.dao;
import java.util.List;

import by.epam.javatr.rdbms.bean.*;
import by.epam.javatr.rdbms.dao.exception.DaoException;

/***
 * interface for methods displaying some basic info 
 * not connected with specific request
 * @author Sergii_Kotov
 *
 */
public interface MutualInfoDao {
	public List<FreeBed> showFreeBeds() throws DaoException;;
	public double get_request_sum(int id) throws DaoException;;
	public BedPriceParametrs getTodayPrice() throws DaoException;;
}
