package by.epam.javatr.rdbms.dao.factory;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Connection {
    static private String user = "root";//Логин пользователя
    static private String password = "masterkey";//Пароль пользователя
    static private String url = "jdbc:mysql://localhost:3306/jdbchotel?autoReconnect=true&useSSL=false";//URL адрес daotalk jdbcHotel useSSL=false
    static private String driver = "com.mysql.jdbc.Driver";//Имя драйвера
    
	static java.sql.Connection connection = null;
    void connect () {
    	try {
				connection = DriverManager.getConnection(url, user, password);
	            Class.forName(driver);//Регистрируем драйвер
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
        }
    }
    
    /***
     * пользователь выбирает доступные места 
     */
    public void showFreeBeds() {
    	String sql ="call get_free_beds();";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
        	System.out.print(String.format("%15s%17s%15s%19s%50s \n",
        	"номер места","состояние места","код заказа","состояние заказа","период использования"));		
            while (rs.next()) {
            	System.out.print(String.format("%15s%17s%15s%19s%50s \n",
            			rs.getInt("num_bed"),rs.getString("bedstatus"), 
            			rs.getString("orderid")==null?" ":rs.getString("orderid"),
            			rs.getString("status")==null?" ":rs.getString("status"),
            			rs.getString("period")==null?" ":rs.getString("period")
            			));
            	
            }            
        } catch (Exception e) {
        }
    }
    
    void execQ(String sql ) {
    	//List<Good> list =new LinkedList<Good>();
        //String sql = "call get_free_beds();";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
        	//Good good = new Good();
            ResultSet rs = statement.executeQuery();
            //list = parseResultSet(rs);
            while (rs.next()) {
            	/*
                good.setInventID(rs.getInt("id"));
                good.setName(rs.getString("department"));
                list.add(good);
                */
            }            
        } catch (Exception e) {
        }
        /*
        for (Good f:list) {
        	System.out.println(f);
        }
        */
    }

    
}
