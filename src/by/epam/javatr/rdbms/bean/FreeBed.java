package by.epam.javatr.rdbms.bean;

import java.io.Serializable;

/***
 * Bean for status of booked and rented beds info 
 * @author Sergii_Kotov
 *
 */
public class FreeBed  implements Serializable {
	private static final long serialVersionUID = 1L;
	private int num_bed;
	private String bedstatus;
	private String orderid;
	private String status;
	private String period;
	
	
	public FreeBed(int num_bed, String bedstatus, String orderid, String status, String period) {
		super();
		this.num_bed = num_bed;
		this.bedstatus = bedstatus;
		this.orderid = orderid;
		this.status = status;
		this.period = period;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bedstatus == null) ? 0 : bedstatus.hashCode());
		result = prime * result + num_bed;
		result = prime * result + ((orderid == null) ? 0 : orderid.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FreeBed other = (FreeBed) obj;
		if (bedstatus == null) {
			if (other.bedstatus != null)
				return false;
		} else if (!bedstatus.equals(other.bedstatus))
			return false;
		if (num_bed != other.num_bed)
			return false;
		if (orderid == null) {
			if (other.orderid != null)
				return false;
		} else if (!orderid.equals(other.orderid))
			return false;
		if (period == null) {
			if (other.period != null)
				return false;
		} else if (!period.equals(other.period))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}


	public int getNum_bed() {
		return num_bed;
	}


	public void setNum_bed(int num_bed) {
		this.num_bed = num_bed;
	}


	public String getBedstatus() {
		return bedstatus;
	}


	public void setBedstatus(String bedstatus) {
		this.bedstatus = bedstatus;
	}


	public String getOrderid() {
		return orderid;
	}


	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getPeriod() {
		return period;
	}


	public void setPeriod(String period) {
		this.period = period;
	}


	@Override
	public String toString() {
		return String.format("%15s%17s%15s%19s%50s \n",
				num_bed,bedstatus,orderid,status,period);
	}
	
}
