package by.epam.javatr.rdbms.bean;

import java.io.Serializable;

public class BedPriceParametrs implements Serializable {
	private static final long serialVersionUID = 1L;
	private double p_rent; 
	private double p_book; 
	private double p_id;
	
	public BedPriceParametrs(double pr, double pb, int pi){
		p_rent=pr;
		p_book=pb;
		p_id=pi;
	}

	@Override
	public String toString() {
		return "Аренда " + p_rent + " денег за место, бронирование одного места " + p_book + " денег";
	}

	public double getP_rent() {
		return p_rent;
	}

	public void setP_rent(double p_rent) {
		this.p_rent = p_rent;
	}

	public double getP_book() {
		return p_book;
	}

	public void setP_book(double p_book) {
		this.p_book = p_book;
	}

	public double getP_id() {
		return p_id;
	}

	public void setP_id(double p_id) {
		this.p_id = p_id;
	}
	
	
	
}
