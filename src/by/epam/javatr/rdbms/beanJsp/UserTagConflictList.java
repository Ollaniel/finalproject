package by.epam.javatr.rdbms.beanJsp;

import javax.servlet.jsp.tagext.TagSupport;
import by.epam.javatr.rdbms.bean.*;


import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import org.apache.log4j.Logger;


public class UserTagConflictList extends TagSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(UserTagConflictList.class.getName());
	private ConflictsList lst;

	public int doStartTag() throws JspException {
		try{
			JspWriter out = pageContext.getOut();
			out.write("<table>");
			out.write("<tr>");
			out.write("<td>ID заказа</td> <td>номер места</td> <td>ID конфликтный</td> <td>дата начала</td> <td>дата конца</td> "
					+ "<td>дата начала конфликта</td> <td>дата конца конфликта</td> <td>код типа</td> <td>код статуса</td> <td>дата создания конфл. заказа</td> ");
			out.write("</tr>");
			
			for(ConflictsInRequest r: lst.getLst()){
				out.write("<tr>");
				out.write(String.format("<td>%s</td> <td>%s</td> "
		       			+ "<td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> "
		       			+ "<td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td>",
		       			r.getReqId(),r.getNum_bed(),r.getConfId(),
		       			r.getDst(),r.getDen(),r.getDstConf(),r.getDenConf(),
		       			r.getConfType(),r.getConfStat(),r.getConfDtCreate()));
				out.write("</tr>");
			}
			out.write("</table>");
		}catch(IOException e){
			logger.error(e);
			throw new JspException(e.getMessage());
		}		
		return SKIP_BODY;
	}

	public ConflictsList getLst() {
		return lst;
	}

	public void setLst(ConflictsList l) {
		this.lst = l;
	}
	
	
}
