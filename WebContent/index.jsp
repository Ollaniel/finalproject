<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="/finaltask/mystyle3.css">
<title>main page</title>
</head>
<body>
	<c:import url="/WEB-INF/js/script.html"/>	
	<c:import url="/header.html"/>	
    <div id="body">
    
	<form id="idShowF" action="SHOWFREE" method="post">
		<input id="idSh" class="submit" name="ShowFreeB" type="submit" value="Show free bed">
	</form>
	<br>
	<br>
	<form id="idReqCr" action="REQUESTCREATE" method="post" style="margin:10px; border: 1px solid powderblue">
		Fill new request parameters
		<div style="margin:10px">
			<div style="margin:10px">
				<input id="id_beds"  name="nbeds" type="text" value="100,105,200,104" />
				<label for="nbeds">	Enter comma separated numbers of the requested bed places </label>
			</div>
			<div style="margin:10px">
				<input id="id_dtStart" name="dtstart" type="date" value="2016-12-30" />
				<label for="id_dtStart"> Start date</label>
			</div>
			<div style="margin:10px">
				<input id="id_dtEnd" name="dtend" type="date" value="2017-02-27" />
				<label for="id_dtEnd"> End date</label>
			</div>
		</div>
		<div>
		<p><input name="typeBook" type="radio" value="1" checked> rent</p>
    	<input name="typeBook" type="radio" value="0"> pre-booking</p>
		</div>
		<input id="idCr" class="submit" name="StartReqCreation" type="submit" value="Start request creation">
		<b><c:out value='${errorText}'/></b>
	</form>
   </div>
   <c:import url="/footer.html"/>
</body>
</html>