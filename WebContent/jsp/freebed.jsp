<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="/finaltask/mystyle3.css">
<title>free beds</title>
</head>
<body>
	<c:import url="/WEB-INF/js/script.html"/>	
	<c:import url="/header.html"/>	
    <div id="body">
    <table>
        <tr>
          <td>номер места<td>
          <td>состояние места<td>
          <td>код заказа<td>
          <td>состояние заказа<td>
          <td>период использования<td>
        </tr>
	<c:forEach items="${bedsList}" var="current">
        <tr>
          <td><c:out value="${current.num_bed}" /><td>
          <td><c:out value="${current.bedstatus}" /><td>
          <td><c:out value="${current.orderid}" /><td>
          <td><c:out value="${current.status}" /><td>
          <td><c:out value="${current.period}" /><td>
        </tr>
    </c:forEach>
    </table>
	<a href="index.jsp">home</a>
   </div>
   <c:import url="/footer.html"/>
</body>
</html>